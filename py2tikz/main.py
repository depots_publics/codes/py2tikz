import re
import matplotlib
import matplotlib.colors
import matplotlib.pyplot as plt
import os
import textwrap
import numpy as np
import shutil
import copy
from PyPDF2 import PdfFileMerger, PdfFileWriter, PdfFileReader
import py2tikz.source as py2tsource

def py2tikz(fig,tex_file_name ='py2tikz_fig',rel_path_to_main_file= '',**kwargs):
    """
    Fonction principale : Enregistre et compile le fichier .tex correspondant à la figure fig
    au chemin "rel_path_to_main_file/tex_file_name/tex_file_name.tex"

    Inputs :
        * fig (type : matplotlib.pylplot.Figure) : figure à exporter
        * tex_file_name (type : str)             : nom du dossier et du fichier .tex
        * rel_path_to_main_file (type : str)     : chemin relatif entre os.cwd() et le dossier d'export
        * kwargs :
            - height (type : str)                : hauteur de l'axe
            - width (type : str)                 : largeur de l'axe
            - grid (type : bool)                 : activation de la grille (major ticks seulement)
            - legend (type : bool)               : activation de la légende (pas possibilité de la place pour l'instant)
            - auto_xticks (type : bool)          : utilisation des xticks automatiques
            - auto_yticks (type : bool)          : utilisation des yticks automatiques
            - type_axis (type : str)             : choix du type d'axe (axis, semilogxaxis, semilogyaxis, loglogaxis)
            - RAZ_figure (type : bool)           : remise à zéro du dossier de la figure
            - split_subplots (type : bool)       : export de chaque subplot dans une figure séparée
            - mappable (type : bool)             : présence d'une colorbar dans la figure à exporter
    """
    # Fonction qui affecte le dictionnaire param_dict à partir des options d'entrée.
    param_dict = handle_kwargs(kwargs)

    # Création des différents chemins utiles dans param_dict
    if rel_path_to_main_file:
        # Chemin vers le dossier de la figure
        param_dict['tikzplot_folder'] = rel_path_to_main_file
    else:
        # Chemin vers le dossier 'tikz'
        param_dict['tikz_folder'] = os.path.join(rel_path_to_main_file,'tikz')
        # Chemin vers le dossier de la figure
        param_dict['tikzplot_folder'] = os.path.join(param_dict['tikz_folder'],tex_file_name)

    # Chemin vers le dossier des données de la figure
    param_dict['data_folder'] = os.path.join(param_dict['tikzplot_folder'],'data')
    # Nom du fichier .tex
    param_dict['tex_file'] = tex_file_name + '.tex'
    # Chemin vers fichier .tex
    param_dict['tikz_location'] = os.path.join(param_dict['tikzplot_folder'],param_dict['tex_file'])

    # Création des répertoires utiles
    create_dirs(param_dict)
    # Ouverture du fichier .tex à écrire
    if not param_dict['split_subplots']:
        print(" ------- Ecriture du fichier ------- ")
        with open(param_dict['tikz_location'],'w+') as tex_file:
            # Ecriture du header (toujours le même)
            py2tsource.write_header(tex_file)
            # Ecriture des axes et des tracés
            nb_axes = len(fig.axes)
            if nb_axes > 1:
                geometries = [ax.get_geometry() for ax in fig.get_axes()]
                tex_file.write("\\begin{groupplot}[\n")
                tex_file.write("group style = {group size = %i by %i,\n"%(geometries[0][1],geometries[0][0]))
                tex_file.write("horizontal sep= 2cm, vertical sep=2cm}, \n")
                if param_dict['type_axis'] == 'semilogxaxis' or param_dict['type_axis'] == 'loglogaxis':
                    tex_file.write("xmode = log, \n")
                if param_dict['type_axis'] == 'semilogyaxis' or param_dict['type_axis'] == 'loglogaxis':
                    tex_file.write("ymode = log, \n")
                tex_file.write("], \n")

                param_dict['subplots'] = True
            else :
                param_dict['subplots'] = False
            if param_dict['mappable']:
                pass
                cb_axe = fig.axes[-1]
                cb_axe.remove()
            for ax in fig.axes:
                py2tsource.write_axis_and_plots(ax,tex_file,param_dict)
                # Fin de l'axe
                if not param_dict['subplots']:
                    tex_file.write('\\end{%s}\n'%param_dict['type_axis'])
            if param_dict['subplots']:
                tex_file.write("\\end{groupplot}\n")

            # Ecriture du footer (toujours le même)
            py2tsource.write_footer(tex_file)
        compile_pdflatex(param_dict['tex_file'],param_dict)
    else:
        # Appels récursif de py2tikz sur les axes des subplots seuls
        nb_axes = len(fig.axes)
        # Objet PdfFileMerger pour fusionner les figures sous forme de pages dans un seul pdf (utile surtout pour vérification)
        pdf_files = PdfFileMerger()
        if nb_axes == 1:
            print("une seule figure à exporter, option split_subplots inutile")
        for i in range(nb_axes-1,-1,-1) :
            # Nouveaux paramètres pour l'appel récursif
            kwargs_recurs = copy.deepcopy(kwargs)
            # Désactivation de l'option 'split_subplots' pour faire une éxecution classique
            kwargs_recurs['split_subplots'] = False
            # Copie de l'axe dans une nouvelle figure temporaire qui sert pour l'appel récursif
            fig_temp = transfer_ax_to_temp_fig(fig.axes[i])
            # Appel récursif avec les mêmes paramètres et la figure temporaire
            subplot_pdf = py2tikz(fig_temp,tex_file_name+'_%i'%i,rel_path_to_main_file,**kwargs_recurs)
            # Supression de la figure temporaire
            plt.close(fig_temp)
            # Ajout de la figure au pdf à fusionner
            pdf_files.append(subplot_pdf[:-4] + '.pdf')
        # Une fois toutes les figures traitées, écriture du pdf fusionné
        pdf_files.write(os.path.join(param_dict['tikz_folder'],"tmp.pdf"))
        pdf_files.close()
        output_pdf = PdfFileWriter()

        with open(os.path.join(param_dict['tikz_folder'],"tmp.pdf"), 'rb') as readfile:
            input_pdf = PdfFileReader(readfile)
            total_pages = input_pdf.getNumPages()
            for page in range(total_pages - 1, -1, -1):
                output_pdf.addPage(input_pdf.getPage(page))
            with open(os.path.join(param_dict['tikz_folder'],tex_file_name + "_merge.pdf"), "wb") as writefile:
                output_pdf.write(writefile)
        os.remove(os.path.join(param_dict['tikz_folder'],"tmp.pdf"))
    if param_dict['mappable']:
        cb_axe.figure = fig
        fig.add_subplot(cb_axe)
    return param_dict['tikz_location']

def transfer_ax_to_temp_fig(ax):
    """
    Transfère un axe d'une figure vers une figure temporaire, pour un appel récursif de py2tikz
    Source :
    https://stackoverflow.com/questions/6309472/matplotlib-can-i-create-axessubplot-objects-then-add-them-to-a-figure-instance/6309636#6309636
    Inputs :
        * fig (type : matplotlib.pylplot.Figure) : figure à exporter
        * ax (type : matplotlib.pyplot.Axe) : axe à exporter
    """

    #ax.remove()
    fig_temp = plt.figure()
    ax.figure = fig_temp
    fig_temp.axes.append(ax)
    fig_temp.add_axes(ax)
    return fig_temp

def compile_pdflatex(file_name,param_dict):
    """
    Compile le fichier tex fourni
    Inputs :
        * tex_file_name (type : str)             : nom du dossier et du fichier .tex
        * param_dict (type : dict) : paramètres d'export
    """
    cwd = os.getcwd()
    # On se place dans le dossier d'export
    os.chdir(param_dict['tikzplot_folder'])
    # Compilation du fichier
    print(" ------- Compilation ------- ")
    if param_dict['compileur'] == 'pdflatex':
        os.system('pdflatex --interaction=batchmode ' + file_name)
    if param_dict['compileur'] == 'lualatex':
        os.system('lualatex --interaction=batchmode ' + file_name)
    # Retour dans le dossier d'exécution
    os.chdir(cwd)
    return

def handle_kwargs(kwargs):
    """
    Fonction qui remplit le dictionnaire param_dict grâce à kwargs ou aux paramètres par défaut
    Inputs :
        * kwargs (type : dict)     : kwargs de la fonction principale
    Outputs :
        * param_dict (type : dict) : paramètres d'export
    """
    default_dict = {'width'          : None,
                    'height'         : None,
                    'grid'           : False,
                    'legend'         : False,
                    'auto_xticks'    : True,
                    'auto_yticks'    : True,
                    'auto_zticks'    : True,
                    'type_axis'      : 'axis',
                    'RAZ_figure'     : False,
                    'split_subplots' : False,
                    'opacity'        : False,
                    'mappable'       : False,
					'compileur'		 : 'pdflatex',
                    'type_figure'    : '2D'
                    }
    param_dict = {}

    """
    Pour chaque keyword :
        - si le keyword existe, on récupère sa valeur
        - type check
        - sinon on utilise la valeur par défaut
    """

    if 'width' in kwargs.keys():
        if isinstance(kwargs['width'],str):
            param_dict['width'] = kwargs['width']
        else:
            param_dict['width'] = default_dict['width']
            print('mauvais type pour la largeur, str attendu')
            print('utilisation de la largeur de la figure matplotlib')
    else:
        param_dict['width'] = default_dict['width']

    if 'height' in kwargs.keys():
        if isinstance(kwargs['height'],str):
            param_dict['height'] = kwargs['height']
        else:
            param_dict['height'] = default_dict['height']
            print('mauvais type pour la hauteur, str attendu')
            print('utilisation de la hauteur de la figure matplotlib')
    else:
        param_dict['height'] = default_dict['height']

    if 'legend' in kwargs.keys():
        if isinstance(kwargs['legend'],bool):
            param_dict['legend'] = kwargs['legend']
        else:
            param_dict['legend'] = default_dict['legend']
            print("mauvais type pour l'activation de la légende, bool attendu")
            print('utilisation de la valeur par défaut, legend = %s'%str(param_dict['legend']))
    else:
        param_dict['legend'] = default_dict['legend']

    if 'auto_xticks' in kwargs.keys():
        if isinstance(kwargs['auto_xticks'],bool):
            param_dict['auto_xticks'] = kwargs['auto_xticks']
        else:
            param_dict['auto_xticks'] = default_dict['auto_xticks']
            print("mauvais type pour l'activation des xticks automatiques, bool attendu")
            print('utilisation de la valeur par défaut, auto_xticks = %s'%str(param_dict['auto_xticks']))
    else:
        param_dict['auto_xticks'] = default_dict['auto_xticks']

    if 'auto_yticks' in kwargs.keys():
        if isinstance(kwargs['auto_yticks'],bool):
            param_dict['auto_yticks'] = kwargs['auto_yticks']
        else:
            param_dict['auto_yticks'] = default_dict['auto_yticks']
            print("mauvais type pour l'activation des yticks automatiques, bool attendu")
            print('utilisation de la valeur par défaut, auto_yticks = %s'%str(param_dict['auto_yticks']))
    else:
        param_dict['auto_yticks'] = default_dict['auto_yticks']

    if 'auto_zticks' in kwargs.keys():
        if isinstance(kwargs['auto_zticks'],bool):
            param_dict['auto_zticks'] = kwargs['auto_zticks']
        else:
            param_dict['auto_zticks'] = default_dict['auto_zticks']
            print("mauvais type pour l'activation des zticks automatiques, bool attendu")
            print('utilisation de la valeur par défaut, auto_zticks = %s'%str(param_dict['auto_zticks']))
    else:
        param_dict['auto_zticks'] = default_dict['auto_zticks']

    if 'type_axis' in kwargs.keys():
        #Options possibles : 'semilogxaxis','semilogyaxis', 'loglogaxis', 'axis'
        if isinstance(kwargs['type_axis'],str) and kwargs['type_axis'] in ['semilogxaxis','semilogyaxis', 'loglogaxis', 'axis','polaraxis','3daxis']:
            param_dict['type_axis'] = kwargs['type_axis']
        else:
            param_dict['type_axis'] = default_dict['type_axis']
            print("mauvais type pour le type d'axe, str attendu parmi ['semilogxaxis','semilogyaxis', 'loglogaxis', 'axis', 'polaraxis','3daxis']")
            print('utilisation de la valeur par défaut, type_axis = %s'%param_dict['type_axis'])
    else:
        param_dict['type_axis'] = default_dict['type_axis']

    if 'RAZ_figure' in kwargs.keys():
        if isinstance(kwargs['RAZ_figure'],bool):
            param_dict['RAZ_figure'] = kwargs['RAZ_figure']
        else:
            param_dict['RAZ_figure'] = default_dict['RAZ_figure']
            print("mauvais type pour l'activation de la RAZ du dossier, bool attendu")
            print('utilisation de la valeur par défaut, RAZ_figure = %s'%str(param_dict['RAZ_figure']))
    else:
        param_dict['RAZ_figure'] = default_dict['RAZ_figure']

    if 'split_subplots' in kwargs.keys():
        if isinstance(kwargs['split_subplots'],bool):
            param_dict['split_subplots'] = kwargs['split_subplots']
        else:
            param_dict['split_subplots'] = default_dict['split_subplots']
            print("mauvais type pour l'export des subplots, bool attendu")
            print('utilisation de la valeur par défaut, split_subplots = %s'%str(param_dict['split_subplots']))
    else:
        param_dict['split_subplots'] = default_dict['split_subplots']

    if 'opacity' in kwargs.keys():
        if isinstance(kwargs['opacity'],bool):
            param_dict['opacity'] = kwargs['opacity']
        else:
            param_dict['opacity'] = default_dict['opacity']
            print("mauvais type pour l'opacité, bool attendu")
            print('utilisation de la valeur par défaut, opacity = %s'%str(param_dict['opacity']))
    else:
        param_dict['opacity'] = default_dict['opacity']

    if 'mappable' in kwargs.keys():
        if isinstance(kwargs['mappable'],bool):
            param_dict['mappable'] = kwargs['mappable']
        else:
            param_dict['mappable'] = default_dict['mappable']
            print("mauvais type pour la présence d'un mappable, bool attendu")
            print('utilisation de la valeur par défaut, mappable = %s'%str(param_dict['mappable']))
    else:
        param_dict['mappable'] = default_dict['mappable']

    if 'compileur' in kwargs.keys():
        if isinstance(kwargs['compileur'],str):
            param_dict['compileur'] = kwargs['compileur']
        else:
            param_dict['compileur'] = default_dict['compileur']
            print("mauvais type pour le compileur, string attendu")
            print('utilisation de la valeur par défaut, compileur = %s'%str(param_dict['compileur']))
    else:
        param_dict['compileur'] = default_dict['compileur']

    if 'type_figure' in kwargs.keys():
        if isinstance(kwargs['type_figure'],str):
            param_dict['type_figure'] = kwargs['type_figure']
        else:
            param_dict['type_figure'] = default_dict['type_figure']
            print("mauvais type pour le type de figure, string attendu ['2D','3D']")
            print('utilisation de la valeur par défaut, type_figure = %s'%str(param_dict['type_figure']))
    else:
        param_dict['type_figure'] = default_dict['type_figure']
    return param_dict

def create_dirs(param_dict):
    """
    Création de tous les répertoires utiles :
    - tikz
    - tikz/tex_file_name
    - tikz/tex_file_name/data
    Inputs :
        * param_dict (type : dict) : paramètres d'export

    """
    try:
        os.mkdir(param_dict['tikz_folder'])
    except:
        pass

    if param_dict['RAZ_figure']:
        shutil.rmtree(param_dict['tikzplot_folder'],ignore_errors=True)

    if not param_dict['split_subplots']:
        try:
            os.mkdir(param_dict['tikzplot_folder'])
        except:
            pass
        try:
            os.mkdir(param_dict['data_folder'])
        except:
            pass
        return
