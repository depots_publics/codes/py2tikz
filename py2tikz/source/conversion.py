import matplotlib.colors
import matplotlib.pyplot as plt

def convert_color_to_html(color):
    """
    Conversion d'une couleur en code HTML hexadécimal
    Inputs :
        * color (type : str)      : tuple RGB, code HTML hexadécimal, couleur nommée de python
    Outputs:
        * color_html (type : str) : code HTML hexadécimal
    """
    # Import du dictionnaire contenant les couleurs nommées
    named_color_mapping = matplotlib.colors.get_named_colors_mapping()
    # Si la couleur est une couleur nommée
    if color in named_color_mapping.keys():
        # On récupère la couleur (qui peut être soit RGB ou HTML selon la couleur)
        color = named_color_mapping[color]
    # Conversion en HTML (dans tous les cas)
    color_html = matplotlib.colors.to_hex(color)
    return color_html

def convert_marker_to_tikz(marker,markersize):
    """
    Conversion d'un type de marker matplotlib en marker tikz + options associées (rotate pour les triangles)
    Inputs :
        * marker (type : str)              : type de marker matplotlib
        * markersize (type : float)        : taille en points du marker matplotlib
    Outputs :
        * marker_all_options (type : list) : liste des options (chaîne de caractères) à utiliser pour le marker
    """
    # Si le marker n'est pas None ni 'None' ni 'none'(les trois sont susceptibles d'apparaître)
    if not marker == None and not marker == 'None' and not marker == 'none':
        # Table de correspondance des markers matplotlib et markers tikz avec options (tiré de tikzplotlib)
        corresp_marker_dict = {
        "x":("x", []),
        "+":("+", []),
        "o":("*", []),
        "v": ("triangle*", ["rotate=180"]),
        "1": ("triangle*", ["rotate=180"]),
        "^": ("triangle*", []),
        "2": ("triangle*", []),
        "<": ("triangle*", ["rotate=270"]),
        "3": ("triangle*", ["rotate=270"]),
        ">": ("triangle*", ["rotate=90"]),
        "4": ("triangle*", ["rotate=90"]),
        "s": ("square*", []),
        "p": ("pentagon*", []),
        "*": ("asterisk", []),
        "h": ("star*", []),
        "H": ("star*", []),
        "d": ("diamond*", []),
        "D": ("diamond*", []),
        "|": ("|", []),
        "_": ("-", []),
        }
        # Récupération du marker tikz avec ses options
        marker_options = corresp_marker_dict[marker]
        # Ecriture du type de marker dans la liste d'options
        marker_all_options = ['mark = %s'%marker_options[0]]
        # option 'solid' pour corriger un bug avec les lignes pointillés
        marker_options[1].append('solid')
        # Ajout de la taille du marker (divisée par deux par rapport à matplotlib)
        marker_options[1].append('mark size = %1.1f'%(markersize*0.5))
        # Si il y a des options à ajouter
        if not marker_options[1] == []:
            # On ajoute une option de au marker
            marker_style_options = 'mark options = {'
            for marker_option in marker_options[1]:
                marker_style_options += marker_option +', '
            marker_style_options = marker_style_options[:-2] + '}'

            # Ajout de l'option mark options à la liste des options
            marker_all_options.append(marker_style_options)
        return marker_all_options

    else:
        # Si il n'y a pas de marker, non pris en compte lors de l'ajout des options
        return [None]

def convert_linestyle_to_tikz(linestyle,linewidth):
    """
    Conversion d'un type de ligne matplotlib en ligne tikz
    Inputs :
        * linestyle (type : str)         : type de ligne matplotlib
        * linewidth (type : float)       : taille en points de la ligne matplotlib

    Outputs :
        * linestyle_option (type : list) : option liée au style de ligne
    """
    # Dictionnaire de correspondance entre lignes matplotlib et tikz (tiré de tikzplotlib)
    corresp_linestyle_dict = {
            "": None,
            "None": None,
            "none": None,
            "-": "solid",
            "solid": "solid",
            ":": "dotted",
            "--": "dashed",
            "-.": "dash pattern=on 1pt off 3pt on 3pt off 3pt",
                            }
    # Récupération du style de ligne
    linestyle_option = corresp_linestyle_dict[linestyle]
    if linestyle_option == None:
        # Si la ligne est None on affiche seulement les markers
        linestyle_option = "only marks"
        linewidth_option = None
    else:
        # Option de largeur de tracé, on introduit le facteur 0.4 entre les échelles en pt
        linewidth_option = 'line width = %1.1f pt'%(0.4*linewidth)

    return [linestyle_option, linewidth_option]

def convert_ticklist_to_str(ticks):
    """
    Convertit les ticks récupérés dans get_ticks() en chaine de caractère utilisable dans tikz :
    Les ticks doivent être de la forme {x1, x2, ..., xn}
    Inputs :
        * ticks (type : list ) : liste contenant l'emplacement des ticks/ticklabels
    Outputs :
        * ticks (type : str) : chaine de caractère contenant tous les ticks à la suite entre accolades
    """

    if not(list(ticks) == []):
        ticks_str = '{'
        for tick in ticks:
            # Si il s'agit d'un plt.Text, on traite des labels, dans ce cas il faut les formater
            if isinstance(tick,plt.Text):
                # Récupération du texte du tick, et remplacement du signe '-' qui est mal encodé
                ticks_str += tick.get_text().replace('−','-').replace('°','\\degree')+ ', '
            else:
                # Ajout du tick
                ticks_str += str(tick) + ', '
        ticks_str = ticks_str[:-2] +  '}'
    else:
        ticks_str = "{}"
    return ticks_str

def convert_cmap_to_tikz(cmap_mpl):

    cm_correspondance = {
     'autumn': 'autumn',
     'cool': 'cool',
     'copper': 'copper',
     'gray': 'blackwhite',
     'hot': 'hot2',
     'hsv': 'hsv',
     'jet': 'jet',
     'spring': 'spring',
     'summer': 'summer',
    "viridis": "viridis",
     'winter': 'winter',
    }
    try:
        cmap_tikz = cm_correspondance[cmap_mpl]
    except:
        print('Colormap %s inexistante en pgfplots, utilisation de viridis par défaut'%cmap_mpl)
        print("Ajouter la colormap manuellement dans le fichier .tex si il s'agit d'une colormap personnalisée")
        cmap_tikz = 'viridis'
    return cmap_tikz
