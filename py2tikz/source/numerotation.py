import os
import re

def get_max_file_current_number(folder):
    """
    Récupère le numéro maximum de fichier data
    Inputs :
        * folder (type : str)             : dossier dans lequel chercher
    Outputs :
        * max_current_number (type : int) : numéro maximal trouvé dans le dossier
    """

    file_list = os.listdir(folder)
    max_current_number = 0
    for file in file_list:
        max_current_number = max(max_current_number,int(file[-7:-4]))
    return max_current_number

def get_max_color_current_number(tex_file):
    """
    Récupère le numéro maximum de couleur
    Inputs :
        * folder (type : str)             : dossier dans lequel chercher
    Outputs :
        * color_max_number (type : int)   : numéro maximal trouvé dans le fichier
    """
    # Curseur en haut du fichier pour la recherche
    tex_file.seek(0)
    # Recherche des couleurs déjà définies
    search_res = re.findall("definecolor{color_.{3}}{HTML}",tex_file.read())
    if search_res != []:
        # Récupération de la dernière couleur existante
        tikz_color_option = re.search("color_.{3}",search_res[-1])
        # Incrémentation du numéro de couleur
        color_max_number = int(tikz_color_option.group(0)[-3:]) + 1
    else :
        color_max_number = 1
    return color_max_number
