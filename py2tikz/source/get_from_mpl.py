from .conversion import convert_ticklist_to_str
import numpy as np
def get_ax_lims(ax,param_dict):
    """
    Récupère les limites d'un axe
    Inputs :
        * ax (type : matplotlib.pyplot.Axe) : axe à exporter
    Outputs :
        * xlims (type : tuple)              : tuple contenant les limites à gauche et à droite de l'axe x
        * ylims (type : tuple)              : tuple contenant les limites en bas et en haut de l'axe y
    """

    xlims = ax.get_xlim()
    if param_dict['type_axis'] == 'polaraxis':
        xlims = [360/2/np.pi*xlims[0],360/2/np.pi*xlims[1]]
    ylims = ax.get_ylim()
    return xlims, ylims

def get_ticks(ax,param_dict):
    """
    Récupère les ticks d'un axe
    Inputs :
        * ax (type : matplotlib.pyplot.Axe) : axe à exporter
    Outputs :
        * xticks (type : str)              : chaine de caractères contenant l'emplacement des xticks
        * xticks_label (type : str)        : chaine de caractères contenant les labels des xticks
        * yticks (type : str)              : chaine de caractères contenant l'emplacement des yticks
        * yticks_label (type : str)        : chaine de caractères contenant les labels des yticks y
    """

    xticks = ax.get_xticks()
    xticks_label = ax.get_xticklabels()

    if param_dict['type_axis'] == 'polaraxis':
        xticks_deg = [360/2/np.pi*xticks[i] for i in range(len(xticks))]
        xticks = xticks_deg

    yticks = ax.get_yticks()
    yticks_label = ax.get_yticklabels()
    return convert_ticklist_to_str(xticks), convert_ticklist_to_str(xticks_label),\
           convert_ticklist_to_str(yticks), convert_ticklist_to_str(yticks_label)

def get_labels(ax):
    """
    Récupère les limites d'un axe
    Inputs :
        * ax (type : matplotlib.pyplot.Axe) : axe à exporter
    Outputs :
        * xlabel (type : str)               : label de l'axe x
        * ylabel (type : str)               : label de l'axe y
    """

    xlabel = ax.get_xlabel()
    ylabel = ax.get_ylabel()

    return xlabel, ylabel

def get_z_axis_info(ax,param_dict):
    zlim = ax.get_zlim()
    zticks = ax.get_zticks()
    zticks_label = ax.get_zticklabels()
    zlabel = ax.get_zlabel()

    return zlim,convert_ticklist_to_str(zticks),convert_ticklist_to_str(zticks_label),zlabel

def get_colorbar_attributes(ax):
    cb = ax.collections[-1].colorbar
    vmin, vmax = cb.vmin,cb.vmax
    cmap_name = cb.cmap.name
    colorbar_label = cb.ax.get_ylabel()
    return vmin,vmax,cmap_name,colorbar_label
