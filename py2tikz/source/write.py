import textwrap
import matplotlib.pyplot as plt
import numpy as np
import matplotlib
import mpl_toolkits
from mpl_toolkits.mplot3d import Axes3D
from mpl_toolkits.mplot3d.art3d import Poly3DCollection
import copy
import re
import os
from .get_from_mpl import get_ax_lims, get_ticks, get_labels, get_colorbar_attributes, get_z_axis_info
from .conversion import convert_color_to_html, convert_marker_to_tikz, convert_linestyle_to_tikz, convert_ticklist_to_str, convert_cmap_to_tikz
from .numerotation import get_max_file_current_number, get_max_color_current_number


def write_header(tex_file):
    """
    Ecriture du header du fichier :
    - import des paquets
    - couleurs poly
    - begin document/tikzpicture

    Pour l'instant le header est toujours le même
    Inputs :
        * tex_file (type : file) : fichier dans lequel écrire
    """
    header_string = """\
                    \\documentclass[12pt]{standalone}
                    %
                    \\usepackage[utf8]{inputenc}
                    \\usepackage[x11names]{xcolor}
                    \\usepackage{tikz}
                    \\usepackage{pgfplots}
                    \\usepackage{gensymb}
                    %
                    \\usepackage{siunitx}
                    \\sisetup{
                    	range-phrase =--,
                        range-units=single,
                    	detect-weight=true,
                    	detect-family=true,
                        detect-display-math = true,
                    		inter-unit-product = \ensuremath{{\cdot}},
                    }
                    %
                    \\usepgfplotslibrary{groupplots}
                    \\usepgfplotslibrary{polar}
                    %
                    \\pgfplotsset{compat=newest}
                    \\definecolor{orgpoly}{RGB}{246,135,18}
                    \\definecolor{rgpoly}{RGB}{191,32,51}
                    \\definecolor{vrpoly}{RGB}{128,204,40}
                    \\definecolor{blpoly}{RGB}{81,173,229}
                    """
    tex_file.write(textwrap.dedent(header_string) + '\n')



    tex_file.write('\\begin{document}\n')
    tex_file.write('%\n')
    tex_file.write('\\begin{tikzpicture}[]\n')
    return

def write_axis_and_plots(ax,tex_file,param_dict):
    """
    Ecriture des axes et des tracés de la figure
    Inputs :
        * fig (type : matplotlib.pylplot.Figure) : figure à exporter
        * tex_file_name (type : str)             : nom du dossier et du fichier .tex
        * param_dict (type : dict)               : paramètres d'export

    """

    # Récupération des limites d'axe
    xlims, ylims = get_ax_lims(ax,param_dict)
    if param_dict['type_figure'] == '3D':
        zlims, zticks, zticks_label, zlabel = get_z_axis_info(ax,param_dict)
    # Récupération des ticks et labels de ticks
    xticks, xticks_label, yticks, yticks_label = get_ticks(ax,param_dict)
    # RécYour locationupération des labels d'axe
    xlabel, ylabel = get_labels(ax)
    # Récupération des paramètres de colorbar
    if param_dict['mappable']:
        vmin,vmax,cmap_name,colorbar_label = get_colorbar_attributes(ax)
    # Ecriture du \begin{axis} avec options
    if param_dict['type_figure'] == '2D':
        write_axis(tex_file,ax,param_dict,xlims,ylims,xticks,xticks_label,yticks,yticks_label,xlabel,ylabel)
    if param_dict['type_figure'] == '3D':
        write_axis(tex_file,ax,param_dict,xlims,ylims,xticks,xticks_label,yticks,yticks_label,xlabel,ylabel, zlims, zticks, zticks_label, zlabel)

    # Ecriture des polycollections (fill_between)
    write_polycollections(tex_file,ax,param_dict)
    # Ecriture des plots et enregistrement des données
    write_plots(tex_file,ax,param_dict)
    # Ecriture des textboxes
    write_texts(tex_file,ax,param_dict)
    # Ecriture des rectangles (barplot)'
    write_rectangle_patches(tex_file,ax,param_dict)
    # Ecriture des mappable (contourf, pcolor)
    if param_dict['mappable']:
        write_mappable(tex_file,ax,param_dict)
    # Si la légende est désactivée, on rajoute une ligne pour qu'elle soit vide (les entrées de légende sont toujours dans le code)
    if param_dict['legend'] == False:
        tex_file.write('\\legend{} %Cette ligne désactive la légende (vide)\n')

    return

def write_axis(tex_file,ax,param_dict,xlims,ylims,xticks,xticks_label,yticks,yticks_label,xlabel,ylabel, zlims=None, zticks=None, zticks_label=None, zlabel=None):
    """
    Ecriture du \begin{axis}
    Inputs :
        * tex_file_name (type : str) : nom du dossier et du fichier .tex
        * param_dict (type : dict)   : paramètres d'export
        * xlims (type : tuple)       : tuple contenant les limites à gauche et à droite de l'axe x
        * ylims (type : tuple)       : tuple contenant les limites en bas et en haut de l'axe y
        * xticks (type : str)        : chaine de caractères contenant l'emplacement des xticks
        * xticks_label (type : str)  : chaine de caractères contenant les labels des xticks
        * yticks (type : str)        : chaine de caractères contenant l'emplacement des yticks
        * yticks_label (type : str)  : chaine de caractères contenant les labels des yticks y
        * xlabel (type : str)        : label de l'axe x
        * ylabel (type : str)        : label de l'axe y
    """

    # Début de l'axe (type d'axe spécifié, par défaut "axis")
    if not param_dict['subplots']:
        tex_file.write('\\begin{%s}[\n'%param_dict['type_axis'])
    if param_dict['subplots']:
        tex_file.write('\\nextgroupplot[\n')

    bbox = ax.get_window_extent().transformed(ax.get_figure().dpi_scale_trans.inverted())
    width, height = bbox.width, bbox.height    # Largeur de l'axe
    if param_dict['width'] == None:
        param_dict['width'] = '%fcm'%(width*2.54)
    tex_file.write('width='+param_dict['width']+',\n')
    # Hauteur de l'axe
    if param_dict['height'] == None:
        param_dict['height'] = '%fcm'%(height*2.54)
    tex_file.write('height='+param_dict['height']+',\n')
    tex_file.write('scale only axis,\n')
    # Limites des axes
    tex_file.write('xmin='+str(xlims[0])+', xmax='+str(xlims[1])+',\n')
    tex_file.write('ymin='+str(ylims[0])+', ymax='+str(ylims[1])+',\n')


    # Si l'option des ticks automatiques est désactivée, on écrit en dur les tick (en écriture décimale pour l'instant)
    # Pour avoir des notations scientifiques, il vaut mieux utiliser les ticks automatiques
    if not(param_dict['auto_xticks']):
        # xticks positionné comme sur la figure matplotlib
        tex_file.write('xtick='+xticks+',\n')
        tex_file.write('xticklabels='+xticks_label+',\n')
        tex_file.write('scaled x ticks = false,\n')
    else:
        # Mêmes lignes mais commentées
        tex_file.write('%xtick='+xticks+',\n')
        tex_file.write('%xticklabels='+xticks_label+',\n')
        tex_file.write('%scaled x ticks = false,\n')
    tex_file.write('xticklabel style={/pgf/number format/fixed, /pgf/number format/precision=5},\n')


    if not param_dict['auto_yticks']:
        # yticks positionné comme sur la figure matplotlib
        tex_file.write('ytick='+yticks+',\n')
        tex_file.write('yticklabels='+yticks_label+',\n')
        tex_file.write('scaled y ticks = false,\n')
    else:
        # Mêmes lignes mais commentées
        tex_file.write('%ytick='+yticks+',\n')
        tex_file.write('%yticklabels='+yticks_label+',\n')
        tex_file.write('%scaled y ticks = false,\n')
        if not(param_dict['type_axis'] =='semilogyaxis') and not(param_dict['type_axis'] =='loglogaxis'):
            log_maxtick = np.log10(np.max(np.abs(ax.get_yticks())))
            scaling_factor = np.floor(log_maxtick)
            if log_maxtick == scaling_factor and 10**log_maxtick > max(ylims):
                scaling_factor -= 1
            tex_file.write('scaled y ticks=base 10:-%i,\n'%scaling_factor)
    tex_file.write('yticklabel style={/pgf/number format/fixed, /pgf/number format/precision=5},\n')

    # Position des ticks
    tex_file.write('xtick pos=left,\n')
    tex_file.write('ytick pos=left,\n')

    # Labels des axes
    tex_file.write('xlabel='+xlabel+',\n')
    tex_file.write('ylabel='+ylabel+',\n')
    # tex_file.write('ylabel style={rotate=-90},\n')

    tex_file.write('scale=1,axis background/.style={fill=white},\n')
    tex_file.write('%axis on top,\n')
    tex_file.write('unbounded coords=jump,\n')


    # Légende fixée pour l'instant (pas idéal)
    tex_file.write('legend style={at={(1,1)}, anchor = north west},\n')
    if param_dict['type_figure'] == '3D':
        tex_file.write('view={%i}{%i}'%(ax.azim+90,ax.elev)+',\n')
        tex_file.write('zlabel='+zlabel+',\n')
        tex_file.write('ztick pos=left,\n')
        tex_file.write('zmin='+str(zlims[0])+', zmax='+str(zlims[1])+',\n')

        if not param_dict['auto_zticks']:
            # zticks positionné comme sur la figure matplotlib
            tex_file.write('ztick='+zticks+',\n')
            tex_file.write('zticklabels='+zticks_label+',\n')
            tex_file.write('scaled z ticks = false,\n')
        else:
            # Mêmes lignes mais commentées
            tex_file.write('%ztick='+zticks+',\n')
            tex_file.write('%zticklabels='+zticks_label+',\n')
            tex_file.write('%scaled z ticks = false,\n')
        tex_file.write('zticklabel style={/pgf/number format/fixed, /pgf/number format/precision=5},\n')

    # Grille
    write_grids(tex_file,ax)

    if param_dict['mappable']:
        vmin,vmax,cmap_name,colorbar_label =  get_colorbar_attributes(ax)
        tex_file.write('colorbar right,\n')
        tex_file.write('colormap name = %s,\n'%cmap_name)
        tex_file.write('point meta min = %f,\n'%vmin)
        tex_file.write('point meta max = %f,\n'%vmax)
        tex_file.write('set layers,\n')
        tex_file.write('colorbar style ={ylabel ='+colorbar_label+',ylabel style={rotate=-90}},')

    tex_file.write(']\n')
    if param_dict['mappable']:
        cmap_tikz_name = convert_cmap_to_tikz(cmap_name)
        tex_file.write('\\pgfplotsset{colormap/%s};\n'%cmap_tikz_name)


    return

def write_plots(tex_file,ax,param_dict):
    """
    Ecriture de tous les tracés (lignes) d'un axe
    Inputs :
        * tex_file_name (type : str)        : nom du dossier et du fichier .tex
        * ax (type : matplotlib.pyplot.Axe) : axe à exporter
        * param_dict (type : dict)          : paramètres d'export
    """
    # On traite chaque ligne une par une
    for line in ax.lines:
        # On récupère le numéro du fichier le plus haut dans le dossier, pour éviter d'écraser des données
        max_current_number = get_max_file_current_number(param_dict['data_folder'])
        # Incrémentation
        file_number = int(max_current_number + 1)
        # Récupération des données XY du tracé

        # Liste des options du tracé qui sera complétée au fur et à mesure
        plot_options = []
        # Récupération de la couleur du tracé et conversion en HTML
        color = convert_color_to_html(line.get_color())
        # Ecriture de la couleur dans le fichier .tex
        tikz_color_option = write_color(tex_file,color)
        # Ajout de la couleur aux options du tracé
        plot_options.append(tikz_color_option)

        # Récupération du style du tracé
        linestyle = line.get_linestyle()
        # Récupération de la largeur du tracé
        linewidth = line.get_linewidth()
        # Conversion du style du tracé matplotlib en tikz
        linestyle_options = convert_linestyle_to_tikz(linestyle,linewidth)
        # Ajout des options de tracé aux options
        for linestyle_option in linestyle_options:
            plot_options.append(linestyle_option)
        # Ajout de l'opacité de la ligne (alpha)
        linealpha = line.get_alpha()
        if linealpha == None:
            linealpha = 1
        if linealpha < 1 and linealpha > 0 and param_dict['opacity'] == True:
            plot_options.append("opacity = %f"%linealpha)

        # Récupération du marker
        marker = line.get_marker()
        markersize = line.get_markersize()
        # Conversion du marker matplotlib en options tikz
        marker_options = convert_marker_to_tikz(marker,markersize)
        # Ajout des options
        for marker_option in marker_options:
            plot_options.append(marker_option)

        # Traitement des options une par une
        plot_options_str ='['
        for option in plot_options:
            # Si une des options est None on ne l'ajoute pas
            if not option == None:
                plot_options_str += str(option) + ', '
        plot_options_str = plot_options_str[:-2] +  ']'
        if not isinstance(line,mpl_toolkits.mplot3d.art3d.Line3D):
            XY = copy.copy(line.get_xydata())
            # Nom du fichier de données associé au tracé
            plot_file_name = os.path.join(param_dict['data_folder'] , 'data_%03i.csv'%(file_number))
            # Enregistrement des données dans le fichier
            if param_dict['type_axis'] == 'polaraxis':
                XY[:,0]*= 360/2/np.pi
            np.savetxt(plot_file_name,XY,delimiter = ",",newline = '\n')
            # Ajout de la ligne du tracé avec les options
            tex_file.write("\\addplot"+plot_options_str+" table[col sep = comma] {data/data_%03i.csv};\n"%(file_number))

        else:
            X,Y,Z = line.get_data_3d()
            plot_file_name = os.path.join(param_dict['data_folder'] , 'data_%03i.csv'%(file_number))
            XYZ = np.zeros((X.shape[0],3))
            XYZ[:,0] = X
            XYZ[:,1] = Y
            XYZ[:,2] = Z
            np.savetxt(plot_file_name,XYZ,delimiter = ",",newline = '\n')
            tex_file.write("\\addplot3"+plot_options_str+" table[col sep = comma] {data/data_%03i.csv};\n"%(file_number))

        # Récupération du label du tracé
        label = line.get_label()
        # Test pour savoir si le label n'est pas le label par défaut : '_lineX'
        if not label.startswith('_'):
            # Ajout d'une entrée de légende
            tex_file.write("\\addlegendentry{%s};\n"%label)


    return

def write_color(tex_file,color):
    """
    Ecriture d'une nouvelle couleur si la couleur fournie n'existe pas dans le fichier
    Inputs :
        * tex_file_name (type : str)        : nom du dossier et du fichier .tex
        * color (type : str)                : code HTML de la couleur
        * file_number (type : int)          : numéro du fichier du tracé
    Outputs :
        * tikz_color_option (type : str)    : alias de la couleur définie dans le code
    """
    # Curseur en haut du fichier pour la recherche
    tex_file.seek(0)
    # Recherche pour savoir si la couleur est déjà définie pour un tracé précédent
    search_res = re.search("\{color_.{3}\}\{HTML\}\{%s\}"%color[1:],tex_file.read())

    if search_res != None:
        # Récupération de la couleur déjà existante
        color_name_string = re.search("color_.{3}",search_res.group(0)).group(0)
    else:
        # Définition d'une nouvelle couleur en préambule
        tex_file.seek(0)
        lines = tex_file.readlines()
        for num_line in range(len(lines)):
            if '\\begin{document' in lines[num_line]:
                num_to_write = num_line - 1
                break
        color_number = get_max_color_current_number(tex_file)
        color_name_string = 'color_%03i'%color_number
        define_color_string = "\\definecolor{%s}{HTML}{%s}\n"%(color_name_string,color[1:])
        lines.insert(num_to_write,define_color_string)
        tex_file.seek(0)
        tex_file.writelines(lines)

    return color_name_string

def write_polycollections(tex_file,ax,param_dict):
    """
    Ecriture de tous les PolyCollection remplis d'un axe, généralement des objets créés par plt.fill_between() (Carter, ..)
    Inputs :
        * tex_file_name (type : str)        : nom du dossier et du fichier .tex
        * ax (type : matplotlib.pyplot.Axe) : axe à exporter
        * param_dict (type : dict)          : paramètres d'export
    """

    # On itère sur tous les enfants de l'axe
    for child in ax.get_children():
        # Si il s'agit d'un PolyCollection
        if isinstance(child,matplotlib.collections.PolyCollection) and not isinstance(child,mpl_toolkits.mplot3d.art3d.Poly3DCollection):
            if child.get_fill():
                # Numéro du fichier à enregistrer
                max_current_number = get_max_file_current_number(param_dict['data_folder'])
                file_number = int(max_current_number + 1)
                path_file_name = os.path.join(param_dict['data_folder'] , 'path_%03i.csv'%(file_number))

                # Récupération des points, de la couleur (rgba)
                polycollection_facecolor_rgba = child.get_facecolor()[0]
                # Séparation de la couleur RGB et alpha, pour la conversion en html
                polycollection_facecolor = (polycollection_facecolor_rgba[0],polycollection_facecolor_rgba[1],polycollection_facecolor_rgba[2])
                polycollection_alpha = polycollection_facecolor_rgba[-1]
                # Conversion de la couleur en HTML
                color_html = convert_color_to_html(polycollection_facecolor)
                # Ecriture de la couleur
                tikz_color_option = write_color(tex_file,color_html)

                polycollection_path = copy.copy(child.get_paths()[0].vertices)
                if param_dict['type_axis'] == 'polaraxis':
                    polycollection_path[:,0]*= 360/2/np.pi
                #Ecriture du fichier
                np.savetxt(path_file_name, polycollection_path, delimiter=",",newline='\n')
                if param_dict['opacity']:
                    # Ajout du tracé fermé rempli avec opacité
                    tex_file.write("\\addplot [draw = none,fill=%s,opacity = %0.2f] table[col sep = comma] {data/path_%03i.csv};\n"%(tikz_color_option,polycollection_alpha,file_number))
                else:
                    # Ajout du tracé fermé rempli sans opacité
                    tex_file.write("\\addplot [draw = none,fill=%s] table[col sep = comma] {data/path_%03i.csv};\n"%(tikz_color_option,file_number))
        elif isinstance(child,mpl_toolkits.mplot3d.art3d.Poly3DCollection):
            if child.get_fill():
                # Numéro du fichier à enregistrer
                max_current_number = get_max_file_current_number(param_dict['data_folder'])
                file_number = int(max_current_number + 1)
                path_file_name = os.path.join(param_dict['data_folder'] , 'path_%03i.csv'%(file_number))

                polycollection_facecolor_rgba = child._facecolors[0]
                # Séparation de la couleur RGB et alpha, pour la conversion en html
                polycollection_facecolor = (polycollection_facecolor_rgba[0],polycollection_facecolor_rgba[1],polycollection_facecolor_rgba[2])
                polycollection_alpha = child.get_alpha()
                # Conversion de la couleur en HTML
                color_html = convert_color_to_html(polycollection_facecolor)
                # Ecriture de la couleur
                tikz_color_option = write_color(tex_file,color_html)
                X = child._vec[0]
                Y = child._vec[1]
                Z = child._vec[2]
                XYZ = np.zeros((X.shape[0],3))
                XYZ[:,0] = X
                XYZ[:,1] = Y
                XYZ[:,2] = Z
                np.savetxt(path_file_name, XYZ, delimiter=",",newline='\n')
                if param_dict['opacity']:
                    # Ajout du tracé fermé rempli avec opacité
                    tex_file.write("\\addplot3 [draw = none,fill=%s,opacity = %0.2f] table[col sep = comma] {data/path_%03i.csv};\n"%(tikz_color_option,polycollection_alpha,file_number))
                else:
                    # Ajout du tracé fermé rempli sans opacité
                    tex_file.write("\\addplot3 [draw = none,fill=%s] table[col sep = comma] {data/path_%03i.csv};\n"%(tikz_color_option,file_number))

    return

def write_mappable(tex_file,ax,param_dict):
    fig = ax.figure
    children = ax.get_children()
    for child in children:
        if isinstance(child,plt.Text):
            child.set_visible(False)
    for line in ax.lines:
        line.set_visible(False)

    # On sauve uniquement le contenu de l'axe en image
    # ref : https://stackoverflow.com/questions/8218608/scipy-savefig-without-frames-axes-only-content
    ax.set_axis_off()
    fig.subplots_adjust(bottom = 0)
    fig.subplots_adjust(top = 1)
    fig.subplots_adjust(right = 1)
    fig.subplots_adjust(left = 0)
    max_current_number = get_max_file_current_number(param_dict['data_folder'])
    file_number = int(max_current_number + 1)
    fig.savefig(param_dict['data_folder']+'/temp_%03i.png'%file_number,bbox_inches='tight',pad_inches=0)
    xlims, ylims = get_ax_lims(ax,param_dict)

    tex_file.write("\\addplot[on layer = axis background] graphics[xmin=%f,ymin=%f,xmax = %f,ymax = %f] {data/temp_%03i};\n"%(xlims[0],ylims[0],xlims[1],ylims[1],file_number))
    ax.set_axis_on()

    for line in ax.lines:
        line.set_visible(True)
    for child in children:
        if isinstance(child,plt.Text):
            child.set_visible(True)
    return

def write_rectangle_patches(tex_file,ax,param_dict):
    fig = ax.figure
    children = ax.get_children()
    for child in children:
        if isinstance(child,matplotlib.patches.Rectangle):
            xy = child.get_xy()
            width = child.get_width()
            height = child.get_height()
            facecolor = child.get_facecolor()
            edgecolor = child.get_edgecolor()
            linewidth = child.get_linewidth()
            linestyle = child.get_linestyle()

            facecolor_rgb = (facecolor[0],facecolor[1],facecolor[2])
            facecolor_html = convert_color_to_html(facecolor_rgb)
            tikz_facecolor_option = write_color(tex_file,facecolor_html)
            facealpha = facecolor[3]
            edgecolor_rgb = (edgecolor[0],edgecolor[1],edgecolor[2])
            edgecolor_html = convert_color_to_html(edgecolor_rgb)
            tikz_edgecolor_option = write_color(tex_file,edgecolor_html)
            edgealpha = edgecolor[3]

            linestyle_options = convert_linestyle_to_tikz(linestyle,linewidth)
            # tex_file.write("\\addplot[patch, patch type=rectangle] coordinates{(%f,%f,)} {data/temp_%03i};\n"%(xlims[0],ylims[0],xlims[1],ylims[1],file_number))
            if not(facecolor_html == '#ffffff' and linewidth==0):

                tex_file.write("\draw[draw = %s,draw opacity = %1.2f,%s,%s,fill = %s,fill opacity = %1.2f] (%f,%f) rectangle (%f,%f);\n"%(tikz_edgecolor_option,min(edgealpha,1),*linestyle_options,tikz_facecolor_option,min(facealpha,1),xy[0],xy[1],xy[0]+width,xy[1]+height))


    return



def write_texts(tex_file,ax,param_dict):
    """
    Ecriture de tous les textes d'un axe
    Inputs :
        * tex_file_name (type : str)        : nom du dossier et du fichier .tex
        * ax (type : matplotlib.pyplot.Axe) : axe à exporter
        * param_dict (type : dict)          : paramètres d'export
    """

    # On itère sur tous les enfants de l'axe
    for child in ax.get_children():
        # Si il s'agit d'un PolyCollection
        if isinstance(child,plt.Text):
            if child.get_text() != '':
                text_color = child.get_color()
                if text_color != 'black':
                    text_color_html = convert_color_to_html(text_color)
                    text_color_name_string = write_color(tex_file,text_color_html)

                    text_options = '[%s, anchor = south west,font=]'%text_color_name_string
                else:
                    text_options = '[anchor = south west]'
                text_fontsize = child.get_size()
                text_options = text_options[:-1] + ',font =\\fontsize{%1.1fpt}{%1.1fpt}\\selectfont]'%(text_fontsize,text_fontsize)
                tex_file.write("\\node%s at (%f,%f) {%s};\n"%(text_options,child.get_position()[0],child.get_position()[1],child.get_text()))
    return

def write_grids(tex_file,ax):
    try:
        major_xgrid = ax.xaxis._gridOnMajor
        minor_xgrid = ax.xaxis._gridOnMinor
        major_ygrid = ax.yaxis._gridOnMajor
        minor_ygrid = ax.yaxis._gridOnMinor
    except:
        major_xgrid = ax.xaxis._major_tick_kw['gridOn']
        minor_xgrid = ax.xaxis._minor_tick_kw['gridOn']
        major_ygrid = ax.yaxis._major_tick_kw['gridOn']
        minor_ygrid = ax.yaxis._minor_tick_kw['gridOn']
    if major_xgrid and major_ygrid and minor_xgrid and minor_ygrid:
        tex_file.write('grid = both,\n')
    elif major_xgrid and major_ygrid and not(minor_xgrid) and not(minor_ygrid):
        tex_file.write('grid = major,\n')
    elif not(major_xgrid) and not(major_ygrid) and minor_xgrid and minor_ygrid:
        tex_file.write('grid = minor,\n')
    else:
        tex_file.write('xmajorgrids = %s,\n'%str(major_xgrid).lower())
        tex_file.write('ymajorgrids = %s,\n'%str(major_ygrid).lower())
        tex_file.write('xminorgrids = %s,\n'%str(minor_xgrid).lower())
        tex_file.write('yminorgrids = %s,\n'%str(minor_ygrid).lower())
    return
def write_footer(tex_file):
    """
    Ecriture du footer du fichier
    Inputs :
        * tex_file (type : file) : fichier dans lequel écrire
    """

    tex_file.write('\\end{tikzpicture}\n')
    tex_file.write('%\n')
    tex_file.write('\\end{document}\n')

    return
