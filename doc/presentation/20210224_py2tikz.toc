\babel@toc {french}{}
\beamer@sectionintoc {1}{Contexte}{3}{0}{1}
\beamer@sectionintoc {2}{Objectifs}{7}{0}{2}
\beamer@sectionintoc {3}{Concurrence}{9}{0}{3}
\beamer@sectionintoc {4}{Cahier des charges}{11}{0}{4}
\beamer@sectionintoc {5}{Principe}{15}{0}{5}
\beamer@sectionintoc {6}{Options}{17}{0}{6}
\beamer@sectionintoc {7}{Exemples}{19}{0}{7}
\beamer@sectionintoc {8}{Documentation}{22}{0}{8}
\beamer@sectionintoc {9}{Base de non-régression}{24}{0}{9}
\beamer@sectionintoc {10}{Contribuer}{28}{0}{10}
\beamer@sectionintoc {11}{Conclusion}{31}{0}{11}
