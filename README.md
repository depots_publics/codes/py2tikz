# Outil de conversion Python 3 vers Tikz/Pgfplots

## Format d'entrée : 
- Figure déjà tracée
- Type de figure (FRF, signal temporel, spectrogramme, ...) : pour la simplicité du code 
- Paramètres d'export

## Format de sortie :

- Fichier *.tex
- Fichiers de données data/*.tsv
- Fichier *.pdf compilé

# Types de figure envisagés 


- Spectrogramme
- Carte d'interaction

# Types de figure fonctionnels

- Tracés simples
- FRF
- Signaux temporels
