import matplotlib.pyplot as plt
import numpy as np
import sys
import os
sys.path.append(os.getcwd())
import py2tikz
import matplotlib
matplotlib.use('Qt4Agg')
xmax = 3*np.pi + np.pi/4
nx = 300
ymax = 3*np.pi + np.pi/4
ny = 300
X = np.linspace(0,xmax,nx)
Y = np.linspace(0,ymax,ny)

XX,YY = np.meshgrid(X,Y)
ZZ = np.cos(XX)*np.sin(2*YY) + 3
fig = plt.figure()
plt.plot([0,3*np.pi],[np.pi*3/4,2*np.pi + np.pi/4],'r--')
plt.plot([np.pi,3*np.pi],[np.pi/4,np.pi + np.pi/4],'r--')
plt.plot([0,3*np.pi],[np.pi*7/4,3*np.pi + np.pi/4],'r--')
plt.text(2,5,'Ceci est un plt.pcolor',color='orange',fontsize = 18)
plt.pcolor(XX,YY,ZZ,cmap='hot')
plt.colorbar()
plt.xlabel('$X$')
plt.ylabel('$Y$')
fig.savefig('base_non_regression/contourplot/contourplot.png')
py2tikz.py2tikz(fig,'contourplot',width = "9cm",height="6cm",auto_xticks = True,auto_yticks = True,RAZ_figure = True,mappable = True)
py2tikz.py2tikz(fig,'contourplot2',width = "9cm",height="6cm",auto_xticks = True,auto_yticks = True,RAZ_figure = True,mappable = True)
