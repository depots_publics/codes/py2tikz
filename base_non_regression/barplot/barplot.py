import matplotlib.pyplot as plt
import numpy as np
import sys
import os
sys.path.append(os.getcwd())
import py2tikz
import matplotlib
from cycler import cycler
matplotlib.use('Qt4Agg')
xmax = 3*np.pi + np.pi/4
nx = 50
colors_poly = {'red_poly':(185/255,30/255,50/255),'orange_poly':(250/255,150/255,30/255),
               'gray_poly':(166/255,168/255,171/255),'green_poly':(140/255,200/255,60/255),
               'blue_poly':(65/255,170/255,230/255)}

colors = [colors_poly['orange_poly'],colors_poly['red_poly'],colors_poly['blue_poly'],
                                colors_poly['green_poly'],colors_poly['gray_poly']]*15
X = np.linspace(0,xmax,nx)

Z = np.cos(X) + 3
fig,ax = plt.subplots()
# ax.set_prop_cycle(custom_cycler)

for i in range(nx):
    if i >25:
        bar=  plt.bar(X[i],Z[i],ec =(1,0,0,1),lw=2,color=(colors[i][0],colors[i][1],colors[i][2],min(1,10/(i+1)+0.5)))
    else:
        bar=  plt.bar(X[i],Z[i],ec =(1,0,0,1),lw=2,linestyle='--',color=(colors[i][0],colors[i][1],colors[i][2],min(1,10/(i+1)+0.5)))

    # print(bar.get_children()[0].get_facecolor())
    # print(bar.get_children()[0].get_edgecolor())
plt.xlabel('$X$')

plt.ylabel('$Y$')
fig.savefig('base_non_regression/barplot/barplot.png')


py2tikz.py2tikz(fig,'barplot',width = "9cm",height="6cm",auto_xticks = True,auto_yticks = True,RAZ_figure = True)
