import matplotlib.pyplot as plt
import numpy as np
import sys
import os
import py2tikz
from matplotlib.ticker import (MultipleLocator, FormatStrFormatter,
                               AutoMinorLocator)
X = np.linspace(0,2*np.pi,100)
coefs = [1,1.05,1.1,1.15,1.2]
Ys = [np.exp(coefs[i]*X) for i in range(5)]

linestyles = ['--','-','-.','','-']
colors = ['b','m',(0/255,170/255,55/255),'#E7A900']
markers = ['>','o','^','+',None]
markersizes = [2,3,4,5,6]
linewidths = [1,1.5,2,2.5,7]
text_colors = ['black','r','#E7A9A0','b',(0/255,170/255,55/255)]
linealpha = [0.4,0.6,0.8,1,0.5,0.75]
fig_lineplots = plt.figure()
fig_lineplots.set_size_inches(9,6)
plt.subplot(221)
for i in range(5):
    if i== 0:
        plt.gca().xaxis.set_minor_locator(AutoMinorLocator())
        plt.grid(which='minor')
    elif i == 1:
        plt.subplot(222)
        plt.grid()
    elif i == 2:
        plt.subplot(223)
        plt.grid(axis ='x')
    elif i == 3 :
        plt.subplot(224)
        plt.grid(axis ='y')
    line = plt.plot(X,Ys[i],linestyle = linestyles[i%len(linestyles)],linewidth = linewidths[i%len(linewidths)],\
             label='$\exp (%1.2fx)$'%coefs[i], color = colors[i%len(colors)],\
             marker=markers[i%len(markers)],markersize =markersizes[i%len(markersizes)],
             alpha = linealpha[i])
    plt.text(5+0.2*i,500,'Hello',color = text_colors[i%len(text_colors)])
    plt.xlim(5,6)
    plt.ylabel('$\exp (ix)$')
    plt.xlabel('$x$')



plt.fill_between(X,Ys[3],Ys[4],color='m',alpha = 0.2)
plt.savefig('base_non_regression/lineplots/lineplots')

py2tikz.py2tikz(fig_lineplots,'lineplots_default')
py2tikz.py2tikz(fig_lineplots,'lineplots',opacity = True, grid = True,legend = False,auto_yticks = True,RAZ_figure = True)
py2tikz.py2tikz(fig_lineplots,'lineplots_sans_opacity',grid = True,legend = False,auto_yticks = True,RAZ_figure = True)
py2tikz.py2tikz(fig_lineplots,'lineplots_sep',opacity = True,grid = True,legend = False,auto_yticks = True,type_axis = 'semilogyaxis',RAZ_figure = True, split_subplots = True)
