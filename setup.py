from setuptools import setup

#with open("README", 'r') as f:
#    long_description = f.read()

setup(
   name='py2tikz',
   version='1.0',
   description='A package to export simple a matplotlib.pyplot.Figure to pgfplots/tikz',
   license="ECL/LAVA",
#   long_description=long_description,
   author='Thibaut Vadcard',
   author_email='thibaut.vadcard@gmail.com',
   url="",
   packages=['py2tikz','py2tikz/source'],  #same as name
   install_requires=['matplotlib','numpy', 'PyPDF2'], #external packages as dependencies
   scripts=['py2tikz/source/conversion.py',
   	    'py2tikz/source/write.py',
   	    'py2tikz/source/get_from_mpl.py',
   	    'py2tikz/source/numerotation.py',
   	    'py2tikz/source/__init__.py']
)
